import React from 'react';
import axios from 'axios';
import { StyleSheet,SafeAreaView, ScrollView, Text, View, TouchableOpacity, StatusBar, ImageBackground  } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input, Button } from 'react-native-elements';
import { useAuth } from '../auth/AuthProvider';

export function Login() {
  const { signIn, signUpForm } = useAuth()
  const [email, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const image = { uri: "https://images.pexels.com/photos/2444852/pexels-photo-2444852.jpeg?cs=srgb&dl=pexels-del-adams-2444852.jpg&fm=jpg" };

  const sign_in=()=>{
    signIn({ email, password })
  }
  const sign_up=()=>{
    signUpForm()
  }
  
  return (
    <View style={styles.container}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <ScrollView>
          <Text style={styles.title}>AVGANGER</Text>
          <View style={styles.form}>
            <Text style={styles.labels}>Correo Electronico</Text>
            <Input
              placeholder='email@address.com'
              errorStyle={{ color: 'red' }}
              style={styles.input}
              errorMessage='ENTER A VALID ERROR HERE'
              leftIcon={{ type: 'font-awesome', name: 'envelope', color: '#6E6D7C', marginRight: 10 }}
            />
            <Text style={styles.labels}>Contraseña</Text>
            <Input
              placeholder='contraseña'
              secureTextEntry={true}
              errorStyle={{ color: 'red' }}
              style={styles.input}
              errorMessage='ENTER A VALID ERROR HERE'
              leftIcon={{ type: 'font-awesome', name: 'lock', color: '#6E6D7C', size:35, marginRight: 10 }}
            />
            
            <Button
              buttonStyle={styles.btn_log}
              titleStyle={styles.text_log_btn}
              onPress={() => sign_in()}
              title="COMENZAR"
            />

            <TouchableOpacity
              onPress={()=>sign_up()}
            >
              <Text style={styles.link}>Crear cuenta</Text>
            </TouchableOpacity>

          </View>
        </ScrollView>
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: '100%',
    height: '100%'
  },
  title: {
    color: "white",
    fontSize: 42,
    lineHeight: 84,
    fontWeight: "bold",
    marginTop: 60,
    textAlign: "center"
  },
  input:{
    color:'white'
  },
  form:{
    marginHorizontal: 30,
    marginTop: 20
  },
  labels:{
    color: 'white',
    fontSize: 18,
    fontWeight:'bold',
    marginLeft:10,
    marginTop:20
  },
  link:{
    color: '#5689CE',
    fontSize:17,
    marginLeft:10,
    marginTop:40,
    fontWeight:'bold'
  },
  btn_log:{
    fontSize:18,
    backgroundColor: '#78281F',
    marginTop:30,
    height:48
  }
});
