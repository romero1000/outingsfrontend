import * as React from 'react'
import { ScrollView, StyleSheet, View, Text, StatusBar } from 'react-native'

export function Notification(){
	return (
		
		<ScrollView style={{backgroundColor:"#7D9198"}}>
			<View style={styles.container}>
				<Text>Bienvenido a Notificaciones</Text>
			</View>
		</ScrollView>
		
	)
}

styles = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:"#7D9198",
		paddingTop: StatusBar.currentHeight,
		justifyContent: 'center',
		alignItems: 'center'
	}
})
