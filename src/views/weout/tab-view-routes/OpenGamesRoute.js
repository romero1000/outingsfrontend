import * as React from 'react'
import { ScrollView, StyleSheet, View, Text, StatusBar, Button} from 'react-native'

export function OpenGamesRoute(){
	return (
		<ScrollView style={{backgroundColor:"#7D9198"}}>
			<View style={styles.container}>
				<Text>Aqui las partidas Abiertas</Text>
			</View>
		</ScrollView>
	)
}

styles = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:"#7D9198",
		paddingTop: StatusBar.currentHeight,
		justifyContent: 'center',
		alignItems: 'center'
	}
})
