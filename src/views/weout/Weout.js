import * as React from 'react';
import { StyleSheet, Dimensions, Text, View, CheckBox } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { OpenGamesRoute } from './tab-view-routes/OpenGamesRoute';
import { StartedGamesRoute } from './tab-view-routes/StartedGamesRoute';
import { CreateGamesRoute } from './tab-view-routes/CreateGamesRoute';
import { Chip } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';

export function Weout() {
  return (
    <TabViewEventDetails/>
  );
}

const initialLayout = { width: Dimensions.get('window').width };

const renderScene = ({ route }) => {
  switch (route.key) {
    case 'first':
      return <OpenGamesRoute />;
    case 'second':
      return <StartedGamesRoute/>
    case 'three':
      return <CreateGamesRoute/>;
    default:
      return null;
  }
};

function TabViewEventDetails() {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'PARTIDAS ABIERTAS', icon:'users' },
    { key: 'second', title: 'PARTIDAS INICIADAS', icon:'hourglass-start' },
    { key: 'three', title: 'CREAR PARTIDA', icon: 'plus-circle'}
  ]);

  const renderTabBar = props => (
    <TabBar
      {...props}
      renderLabel={({ route, focused, color }) => (
  	    <Text style={{ color, margin: 8, fontWeight:'bold', justifyContent:'center', fontSize: 9 }}>
  	      {route.title}
  	    </Text>
  	  )}
      renderIcon={({ route, focused, color }) => (
        <Icon
          name={route.icon}
          color={color}
          size={24}
        />
      )}
      activeColor={'#000'}
      inactiveColor={'#808C7F'}
      indicatorStyle={{ backgroundColor: 'black' }}
      style={{ backgroundColor: '#353D48'}}
    />
  );

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
    />
  );
}
