import * as React from 'react'
import { ScrollView, StyleSheet, View, Text, StatusBar, Button, Image } from 'react-native'
import { useAuth } from '../../auth/AuthProvider';
import * as ImagePicker from 'expo-image-picker';
import axios from 'axios'
import Global from '../../../enviroment/Global'
import FormData from 'form-data'
import mime from 'mime'

export function Profile(){

	const { signOut } = useAuth()

	const [image, setImage] = React.useState(null)
	const [img_result, setImgResult] = React.useState(null)

	React.useEffect(() => {
		(async () => {
		  if (Platform.OS !== 'web') {
		    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
		    if (status !== 'granted') {
		      alert('Sorry, we need camera roll permissions to make this work!');
		    }
		  }
		})();
	}, []);

	const pickImage = async () => {
		let result = await ImagePicker.launchImageLibraryAsync({
		  mediaTypes: ImagePicker.MediaTypeOptions.All,
		  allowsEditing: true,
		  aspect: [4, 3],
		  quality: 1,
		});
		setImgResult(result)
		console.log(result);

		if (!result.cancelled) {
		  setImage(result.uri);
		}
	};

	const getAvatarUser = async () =>{
		let userId = "612c014c82954d3f787b43e8"
		
		await axios
		.get(Global.api+'/user/'+userId)
		.then((response)=>{
			let image_uri = Global.api+'/user/get-image/'+response.data.user.avatar
			console.log(image_uri)
			setImage(image_uri);
		})
		.catch((error)=>{
			console.log(error)
		})
		.finally(()=>{
			console.log("Finalizado")
		})
	}

	const uploadImg = async () =>{
		const newImageUri =  "file:///" + img_result.uri.split("file:/").join("");
		if(img_result){
			const formData = new FormData();
			formData.append('avatar', {
				uri : newImageUri,
				type: mime.getType(newImageUri),
				name: newImageUri.split("/").pop()
			});
			
			await axios
			.post(Global.api+'/user/upload-avatar/612a4941d5613933e4fb678d', formData, {
				headers: {
					'Content-Type': 'multipart/form-data'
				}
			})
			.then((response)=>{
				console.log("====RESPONSE====")
				console.log(response)
			})
			.catch((err)=>{
				console.log("====ERROR====")
				console.log(err)
			})
			.finally(()=>{
				console.log("=====FINALIZADO====")
			})
		}else{
			alert("Seleccion una Imagen")
		}
	}

	return (
		
			<View style={[styles.container, {
		      // Try setting `flexDirection` to `"row"`.
		      flexDirection: "column"
		    }]}>
				<View style={{flex:1}}>
					<Text>Bienvenido a su Perfil</Text>
					<Text>Signed in!</Text>
	        		<Button title="Sign out" onPress={signOut}/>
				</View>
				<View style={{flex:2}}>
					<Button title="Pick an image from camera roll" onPress={pickImage} />
      				{image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
					<Button title="Upload" onPress={uploadImg} />
				</View>
				<View style={{flex:3}}>
					<Button title="test" onPress={getAvatarUser} />
				</View>
			</View>
		
	)
}

styles = StyleSheet.create({
	container:{
		backgroundColor:"#7D9198",
		flex:1,
		paddingTop: StatusBar.currentHeight,
		padding: 10
		// justifyContent: 'center',
		// alignItems: 'center'
	}
})
