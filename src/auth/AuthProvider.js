import * as React from 'react';
import { Button, Text, TextInput, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import axios from 'axios'
import Global from '../../enviroment/Global'

const AuthContext = React.createContext();

export function AuthProvider( {children} ) {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
            isSignUpForm: false,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
            isSignUpForm:false
          };
        case 'FORM_SIGN_UP':
          return {
            ...prevState,
            isSignout:false,
            isSignUpForm: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      isSignUpForm: false,
      userToken: null,
    }
  );

  React.useEffect(() => {
    // Obtener el token del almacenamiento y luego navegar a nuestro lugar apropiado
    const bootstrapAsync = async () => {
      let userToken;

      try {
        // Restaure el token almacenado en `SecureStore` o cualquier otro almacenamiento encriptado
        // userToken = aguardar SecureStore.getItemAsync ('userToken');
      } catch (e) {
        // Restoring token failed
      }

      // Después de restaurar el token, es posible que debamos validarlo en aplicaciones de producción

      // Esto cambiará a la pantalla de la aplicación o la pantalla de autenticación y esta carga
      // la pantalla se desmontará y se desechará.
      dispatch({ type: 'RESTORE_TOKEN', token: userToken });
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      state: state,
      signIn: async (data) => {
        // En una aplicación de producción, necesitamos enviar algunos datos
        // (generalmente nombre de usuario, contraseña) al servidor y obtener un token
        // También necesitaremos manejar los errores si el inicio de sesión falló
        // Después de obtener el token, necesitamos conservar el token usando `SecureStore` o cualquier otro almacenamiento encriptado
        // En el ejemplo, usaremos un token ficticio

        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
      signOut: () => dispatch({ type: 'SIGN_OUT' }),
      signUp: async (data) => {
        console.log(data)
        axios
        .post(Global.api+"/user/sign_up", data)
        .then((response) => {
          console.log("===========RESPONSE==============0")
          let token = response.data.token
          dispatch({ type: 'SIGN_IN', token: token });
        })
        .catch((error)=>{
          console.log("=========ERROR==========")
          alert("HUBO UN ERROR")
          console.log(error)
        })
        .finally(()=>{
          console.log("====FINALIZADO======")
        })
        // En una aplicación de producción, necesitamos enviar los datos del usuario al servidor y obtener un token
        // También necesitaremos manejar los errores si el registro falló
        // Después de obtener el token, necesitamos conservar el token usando `SecureStore` o cualquier otro almacenamiento encriptado
        // En el ejemplo, usaremos un token ficticio
      },
      signUpForm: () => dispatch({type: 'FORM_SIGN_UP'}),
    }),
    [state]
  );


  return (
    <AuthContext.Provider value={authContext}>
      {children}
    </AuthContext.Provider>
  );
}

// {state.isLoading ? (
//             // Aún no hemos terminado de buscar el token
//             <SplashScreen/>
//           ) : ((state.userToken == null) & (state.isSignUpForm == false)) ? (
//             // No se encontró ningún token, el usuario no inició sesión
//             <Login authContext={AuthContext}/>
//           ) : ((state.userToken == null) & (state.isSignUpForm == true)) ? (
//             <SignUp authContext={AuthContext}/>
//           ): (
//             // El usuario ha iniciado sesión
//             <Navigation/>
//           )}

export const useAuth = () =>{

  const context = React.useContext(AuthContext)

  if(!context){
    throw new Error('useAuth Debe estar dentro del proveedor Auth')
  }
  return context
}
