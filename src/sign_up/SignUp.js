import * as React from 'react';
import { useState, useEffect } from 'react';
import { Input, Button, errorMessage, CheckBox } from 'react-native-elements';
import { View,Text, StyleSheet, Alert, ScrollView } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { size } from 'lodash';
import { useAuth } from '../auth/AuthProvider'  

export function SignUp() {
  const { signOut, signUp } = useAuth()

  const [formData, setFormData] = useState({
    name: "",
    username: "",
    email: "",
    password: "",
    age: "",
    gender:"Masculino",
    region:"",
    country:"",
    lenguague:"",
    avatar: "null"
  })

  const [errorName, setErrorName] = useState("")
  const [errorUsername, setErrorUsername] = useState("")
  const [errorEmail, setErrorEmail] = useState("")
  const [errorPassword, setErrorPassword] = useState("")
  const [errorAge, setErrorAge] = useState("")
  const [male, setMale] = useState(true)
  const [female, setFemale] = useState(false)
  const [errorRegion, setErrorRegion] = useState("")
  const [errorCountry, setErrorCountry] = useState("")
  const [errorlenguague, setErrorlenguague] = useState("")
  
  useEffect(() => {
  }, []);

  const onChange = (e, type) => {
    setFormData({ ...formData, [type]: e.nativeEvent.text })
  }

  const onChangeGender = (gender) =>{
    setFormData({ ...formData, gender:gender })
  }

  const doRegisterUser = async () => {
    signUp(formData)
  }

  const validateData = () => {
    setErrorName("")
    setErrorUsername("")
    setErrorEmail("")
    setErrorPassword("")
    setErrorAge("")
    setGender("")
    setErrorRegion("")
    setErrorCountry("")
    setErrorlenguague("")

    let isValid = true

    if (!(formData.name) || /^\s*$/.test(formData.name) || size(formData.name) > 25) {
      setErrorName("Debes de ingresar un nombre válido, maximo 15 caracteres.")
      isValid = false
    }

    if (!(formData.username) || /^\s*$/.test(formData.username) || size(formData.username) > 25) {
      setErrorUsername("Debes de ingresar un lugar válido, maximo 25 caracteres.")
      isValid = false
    }

    if (!(formData.email) || (todayDate > (formData.email))) {
      setErrorEmail("Debes de ingresar una fecha valida")
      isValid = false
    }else{
      if(todayDate == formData.email){
        if (!(formData.password) || (todayHour > (formData.password))) {
          setErrorPassword("Debes de ingresar una hora valida")
          isValid = false
        }
      }
    }
    
    if (!(formData.password)) {
      setErrorPassword("Debes de ingresar una hora valida")
      isValid = false
    }



    if (!(formData.age) || /^\s*$/.test(formData.age) || size(formData.age) > 156) {
      setErrorAge("Debes de ingresar una descripcion.")
      isValid = false
    }

    return isValid
  }

  return (
    <ScrollView style={styles.container}>
    <Text style={{fontSize: 30, fontWeight:'bold', color:'black', padding: 24,textDecorationLine: 'underline', marginLeft: 12}}>Registro de Usuario</Text>
      <View>

          <Input
            style={styles.input}
            placeholder="Nombres y Apellidos"
            placeholderTextColor="#343C47"
            leftIcon={
              <MaterialCommunityIcons name="account" size={27} color="#343C47" />
            }
            errorMessage={errorName}
            onChange={(e) => onChange(e, "name")}
            defaultValue={formData.name}
          />

          <Input
            style={styles.input}
            placeholderTextColor="#343C47"
            placeholder="Nombre de Usuario"
            leftIcon={
              <MaterialCommunityIcons name="account-arrow-right" size={27} color="#343C47" />
            }
            errorMessage={errorUsername}
            onChange={(e) => onChange(e, "username")}
            defaultValue={formData.username}

          />

          <Input
            style={styles.input}
            placeholderTextColor="#343C47"
            placeholder="Email"
            leftIcon={
              <MaterialCommunityIcons name="email" size={27} color="#343C47" />
            }
            defaultValue={formData.email}
            errorMessage={errorEmail}
            onChange={(e) => onChange(e, "email")}
            keyboardType={'email-address'}
          />

          <Input
            style={styles.input}
            placeholderTextColor="#343C47"
            placeholder="Contraseña"
            leftIcon={
              <MaterialCommunityIcons name="lock" size={27} color="#343C47" />
            }
            onChange={(e) => onChange(e, "password")}
            defaultValue={formData.password}
            errorMessage={errorPassword}
          />
          
          <View style={styles.raddiobtn}>
            
            <CheckBox
              title='Masculino'
              checkedIcon='dot-circle-o'
              checkedColor= 'black'
              uncheckedIcon='circle-o'
              containerStyle={{backgroundColor: '#81999D'}}
              textStyle={{color:'#343C47'}}
              onPress={()=>{
                setMale(true)
                setFemale(false)
                onChangeGender('Masculino')
              }}
              checked={male}
            />
            <CheckBox
              style={styles.input}
              title='Femenino'
              checkedColor= 'black'
              checkedIcon='dot-circle-o'
              textStyle={{color:'#343C47'}}
              containerStyle={{backgroundColor: '#81999D'}}
              uncheckedIcon='circle-o'
              onPress={()=>{
                setFemale(true)
                setMale(false)
                onChangeGender('Femenino')
              }}
              checked={female}
            />
          </View>

          <Input
            style={styles.input}
            placeholderTextColor="#343C47"
            keyboardType={'numeric'}
            placeholder="Edad"
            leftIcon={
              <MaterialCommunityIcons name="face" size={27} color="#343C47" />
            }
            errorMessage={errorAge}
            defaultValue={formData.age}
            onChange={(e) => onChange(e, "age")} 
          />
      

          <Input
            style={styles.input}
            placeholder="Region"
            placeholderTextColor="#343C47"
            leftIcon={
              <MaterialCommunityIcons name="map-marker" size={27} color="#343C47" />
            }
            errorMessage={errorRegion}
            defaultValue={formData.region}
            onChange={(e) => onChange(e, "region")}
          />

          <Input
            style={styles.input}
            placeholderTextColor="#343C47"
            placeholder="Pais"
            leftIcon={
              <MaterialCommunityIcons color='white' name="flag" size={27} color="#343C47" />
            }
            errorMessage={errorCountry}
            defaultValue={formData.country}
            onChange={(e) => onChange(e, "country")}
          />

          <Input
            style={styles.input}
            placeholderTextColor="#343C47"
            placeholder="Idioma"
            leftIcon={
              <MaterialCommunityIcons name="account-voice" size={27} color="#343C47" />
            }
            errorMessage={errorlenguague}
            defaultValue={formData.lenguague}
            onChange={(e) => onChange(e, "lenguague")}
          />

          <Button
            buttonStyle={styles.butoonP}
            titleStyle={styles.textButtonP}
            onPress={() => doRegisterUser()}
            title="REGISTRARSE"
          />

          <Button
            buttonStyle={styles.butoonC}
            titleStyle={styles.textButtonC}
            onPress={signOut}
            title="CANCELAR"
          />
        
      </View>
    </ScrollView>
  )
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7D9198',
    // marginTo p:30,
    paddingLeft: 20,
    paddingRight: 20,
    marginTop:30
  },
  butoonP: {
    backgroundColor: '#000000',
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15

  },

  textButtonP: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold'
  },

  butoonC: {
    backgroundColor: '#353D48',
    borderColor: '#000000',
    borderWidth: 2,
    marginBottom: 8,
    paddingTop: 9,
    paddingBottom: 9,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15

  },

  textButtonC: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold'
  },

  input: {
    width: "100%",
    fontSize: 20,
    // fontWeight: 'bold',
    fontStyle: 'italic' ,
    //borderBottomWidth:1
    //backgroundColor:'green'
  },
  raddiobtn:{
    flexDirection: 'row',
    justifyContent:'center',
  },
  icon: {
    alignItems: 'center',
  },

  botones: {
    display: 'flex',
    margin: 5,
    padding: 5,
    backgroundColor: '#000000'
  },
})

//underlineColorAndroid="black"