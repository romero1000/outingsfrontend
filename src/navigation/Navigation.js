import * as React from 'react'
import { Text, View,Button } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { Profile } from '../views/profile/Profile'
import { Home } from '../views/home/Home'
import { Notification } from '../views/notification/Notification'
import { Weout } from '../views/weout/Weout'

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        "tabBarActiveTintColor": "#353D48",
        "tabBarStyle": [
          {
            "display": "flex",
            backgroundColor:'black',
          },
          null
        ]
      }}
    >
      <Tab.Screen
        name="Inicio"
        component={Home}
        options={{
          headerShown: true,
          headerTitleAlign: 'center',
          headerTitleStyle: { fontSize:20, color:'white'  },
          headerStyle: {
            backgroundColor: 'black',
            height:70
          },
          tabBarLabel: 'Inicio',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Partidas"
        component={Weout}
        options={{
          headerShown: true,
          headerTitleAlign: 'center',
          headerTitleStyle: { fontSize:20, color:'white'  },
          headerStyle: {
            backgroundColor: 'black',
            height:70
          },
          tabBarLabel: 'Salidas',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="human" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Notificaciones"
        component={Notification}
        options={{
          headerShown: true,
          headerTitleAlign: 'center',
          headerTitleStyle: { fontSize:20, color:'white'  },
          headerStyle: {
            backgroundColor: 'black',
            height:70
          },
          tabBarLabel: 'Notificaciones',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="bell" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Perfil"
        component={Profile}
        options={{
          headerShown: true,
          headerTitleAlign: 'center',
          headerTitleStyle: { fontSize:20, color:'white'  },
          headerStyle: {
            backgroundColor: 'black',
            height:70
          },
          tabBarLabel: 'Perfil',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
export function Navigation() {
  return (
    <NavigationContainer>
      <MyTabs />
    </NavigationContainer>
  );
}
