import React from 'react';
import { View, Text } from 'react-native'
import { Login } from './src/login/Login';
import {Navigation} from './src/navigation/Navigation'
import { SignUp } from './src/sign_up/SignUp'
import { AuthProvider, useAuth } from './src/auth/AuthProvider';

function SplashScreen() {
  return (
    <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
      <Text>Loading...</Text>
    </View>
  );
}

function ScreenInitializer() {
  const { state } = useAuth()

  console.log("-----------------INIT-----------------")
  console.log(state)

  if( state.isLoading){
    return <SplashScreen/>
  }else if((state.userToken == null) & (state.isSignUpForm == false)){
    return <Login/>
  }else if((state.userToken == null) & (state.isSignUpForm == true)){
    return <SignUp/>
  }else{
    return <Navigation/>
  }
}

export default function App() {
  return (
    <AuthProvider>
      <ScreenInitializer/>
    </AuthProvider>
  )
}
